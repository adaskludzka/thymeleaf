package pl.skludzka.thymeleaf1;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.concurrent.CopyOnWriteArrayList;

@Controller
public class EmployeeController {

    private CopyOnWriteArrayList<Employee> employees = new CopyOnWriteArrayList<>();

    @GetMapping("/home")
    public String goHome (Model model){
        model.addAttribute("employees", employees);
        return "home";

    }

    @GetMapping("/add")
    public String goToAdd(Model model){
        model.addAttribute("employee", new Employee());
        return "add_employee";
    }

    @PostMapping("/addEmployee")
    public String addEmployee(@ModelAttribute Employee employee){
        employees.add(employee);
        return"redirect:/home";
    }

}
