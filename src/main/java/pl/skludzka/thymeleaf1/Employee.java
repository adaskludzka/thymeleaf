package pl.skludzka.thymeleaf1;

import lombok.Data;

@Data
public class Employee {


    private String name;
    private String surname;
    private String age;
    private String department;



    private final String[] departments = {"Biuro", "Chłodnia", "Transport"};


}
